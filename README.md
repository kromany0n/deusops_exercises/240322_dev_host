# DEV контур для задачи 240322

## Требования

- Нужен сервер с установленным Docker
- Доступ к портам 9100, 9080, 9082 c адреса сборщика метрик (vmagent, prometheus).
- Доступ к портам 80 и 443

## Описание

В качетстве reverse-proxy используется traefik. Web-интерфейс доступен по адресу <https://dev.todo-list-app.deusops.kromanyon.ru> c HTTP Baisic авторизацией. для того, чтобы в gitlab-ci удобнее было привязывать сети dev-окружений к trafeik, ему задан container_name в docker-compose.yml.

Ресурсы сервера мониторим node_exporter-ом, а контейнеры Docker при помощи cAdvisor. Интерфейс cAdvisor при необходимости можно увидеть на <https://cadvisor.dev.todo-list-app.deusops.kromanyon.ru>. Доступы те же что и к trafeik.

## Переменные .env

```bash
REGRU_USERNAME=xx@example.com
REGRU_PASSWORD=xxxxxxxxxx
ACME_EMAIL=xx@example.com
TRAEFIK_TLS_DOMAIN_BASE=dev.todo-list-app.deusops.kromanyon.ru
TRAEFIK_API_INSECURE=true
TRAEFIK_LOG_LEVEL="DEBUG"
```

## Доступы

Передам в комментарии к задаче.
